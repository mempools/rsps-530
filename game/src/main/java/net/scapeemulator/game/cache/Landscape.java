package net.scapeemulator.game.cache;

import net.scapeemulator.cache.def.ObjectDefinition;
import net.scapeemulator.cache.util.ByteBufferUtils;
import net.scapeemulator.game.model.GroundObject;
import net.scapeemulator.game.model.Position;
import net.scapeemulator.game.model.World;
import net.scapeemulator.game.model.ObjectDefinitions;
import net.scapeemulator.game.model.ObjectDefinitions.ObjectType;


import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public final class Landscape {

	public static Landscape decode(int x, int y, ByteBuffer buffer) {
		Landscape landscape = new Landscape(x, y);

		int id = -1;
		int deltaId;

		while ((deltaId = ByteBufferUtils.getSmart(buffer)) != 0) {
			id += deltaId;

			int pos = 0;
			int deltaPos;

			while ((deltaPos = ByteBufferUtils.getSmart(buffer)) != 0) {
				pos += deltaPos - 1;

				int localX = (pos >> 6) & 0x3F;
				int localY = pos & 0x3F;
				int height = (pos >> 12) & 0x3;

				int temp = buffer.get() & 0xFF;
				int type = (temp >> 2) & 0x3F;
				int rotation = temp & 0x3;

				Position position = new Position(x * 64 + localX, y * 64 + localY, height);
				objectDecoded(id, rotation, ObjectType.forId(type), position);
			}
		}

		return landscape;
	}public static void objectDecoded(int id, int rotation, ObjectType type, Position position) {
		ObjectDefinition def = ObjectDefinitions.forId(id);
		if(!def.isSolid()) {
			return;
		}

		if(!World.getWorld().getTraversalMap().regionInitialized(position.getX(), position.getY())) {
			World.getWorld().getTraversalMap().initializeRegion(position.getX(), position.getY());
		}

		if(type.isWall()) {
			World.getWorld().getTraversalMap().markWall(rotation, position.getHeight(), position.getX(), position.getY(), type, def.isImpenetrable());
		}

		if(type.getId() >= 9 && type.getId() <= 12) {

            /* Flip the length and width if the object is rotated */
			int width = def.getWidth();
			int length = def.getLength();
			if(1 == rotation || rotation == 3) {
				width = def.getLength();
				length = def.getWidth();
			}

			World.getWorld().getTraversalMap().markOccupant(position.getHeight(), position.getX(), position.getY(), width, length, def.isImpenetrable());
		}
	}

	private final int x, y;
	private final List<GroundObject> objects = new ArrayList<>();

	private Landscape(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public List<GroundObject> getObjects() {
		return objects;
	}

}
