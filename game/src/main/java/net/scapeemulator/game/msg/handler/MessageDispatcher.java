package net.scapeemulator.game.msg.handler;

import net.scapeemulator.game.button.ButtonDispatcher;
import net.scapeemulator.game.model.Player;
import net.scapeemulator.game.msg.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public final class MessageDispatcher {

	private static final Logger logger = LoggerFactory.getLogger(MessageDispatcher.class);

	private final Map<Class<?>, MessageHandler<?>> handlers = new HashMap<>();
	private final ButtonDispatcher buttonDispatcher = new ButtonDispatcher();

	public MessageDispatcher() {
		bind(PingMessage.class, new PingMessageHandler());
		bind(IdleLogoutMessage.class, new IdleLogoutMessageHandler());
		bind(ButtonMessage.class, new ButtonMessageHandler(buttonDispatcher));
		bind(WalkMessage.class, new WalkMessageHandler());
		bind(ChatMessage.class, new ChatMessageHandler());
		bind(CommandMessage.class, new CommandMessageHandler());
		bind(ExtendedButtonMessage.class, new ExtendedButtonMessageHandler(buttonDispatcher));
		bind(SwapItemsMessage.class, new SwapItemsMessageHandler());
		bind(EquipItemMessage.class, new EquipItemMessageHandler());
		bind(DisplayMessage.class, new DisplayMessageHandler());
		bind(RemoveItemMessage.class, new RemoveItemMessageHandler());
		bind(RegionChangedMessage.class, new RegionChangedMessageHandler());
		bind(ClickMessage.class, new ClickMessageHandler());
		bind(FocusMessage.class, new FocusMessageHandler());
		bind(CameraMessage.class, new CameraMessageHandler());
		bind(FlagsMessage.class, new FlagsMessageHandler());
		bind(SequenceNumberMessage.class, new SequenceNumberMessageHandler());
		bind(InterfaceClosedMessage.class, new InterfaceClosedMessageHandler());
	}

	public <T extends Message> void bind(Class<T> clazz, MessageHandler<T> handler) {
		handlers.put(clazz, handler);
	}

	@SuppressWarnings("unchecked")
	public void dispatch(Player player, Message message) {
		MessageHandler<Message> handler = (MessageHandler<Message>) handlers.get(message.getClass());
		if (handler != null) {
			try {
				handler.handle(player, message);
			} catch (Throwable t) {
				logger.warn("Error processing packet.", t);
			}
		} else {
			logger.warn("Cannot dispatch message (no handler): " + message.getClass().getName() + ".");
		}
	}

}
