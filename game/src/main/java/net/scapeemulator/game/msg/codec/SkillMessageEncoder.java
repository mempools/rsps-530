package net.scapeemulator.game.msg.codec;

import io.netty.buffer.ByteBufAllocator;
import net.scapeemulator.game.msg.SkillMessage;
import net.scapeemulator.game.net.game.*;

public final class SkillMessageEncoder extends MessageEncoder<SkillMessage> {

	public SkillMessageEncoder() {
		super(SkillMessage.class);
	}

	@Override
	public GameFrame encode(ByteBufAllocator alloc, SkillMessage message) {
		GameFrameBuilder builder = new GameFrameBuilder(alloc, 38);
		builder.put(DataType.BYTE, DataTransformation.ADD, message.getLevel());
		builder.put(DataType.INT, DataOrder.MIDDLE, message.getExperience());
		builder.put(DataType.BYTE, message.getSkill());
		return builder.toGameFrame();
	}

}
