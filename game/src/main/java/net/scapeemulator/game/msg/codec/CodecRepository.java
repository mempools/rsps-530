package net.scapeemulator.game.msg.codec;

import net.scapeemulator.game.msg.Message;
import net.scapeemulator.game.util.LandscapeKeyTable;

import java.util.HashMap;
import java.util.Map;

public final class CodecRepository {

	private final MessageDecoder<?>[] inCodecs = new MessageDecoder<?>[256];
	private final Map<Class<?>, MessageEncoder<?>> outCodecs = new HashMap<>();

	public CodecRepository(LandscapeKeyTable table) {
		/* decoders */
		bind(new PingMessageDecoder());
		bind(new IdleLogoutMessageDecoder());
		bind(new ButtonMessageDecoder());
		bind(new WalkMessageDecoder(39));
		bind(new WalkMessageDecoder(77));
		bind(new WalkMessageDecoder(215));
		bind(new ChatMessageDecoder());
		bind(new CommandMessageDecoder());
		bind(new ExtendedButtonMessageDecoder());
		bind(new SwapItemsMessageDecoder());
		bind(new EquipItemMessageDecoder());
		bind(new DisplayMessageDecoder());
		bind(new RemoveItemMessageDecoder());
		bind(new RegionChangedMessageDecoder());
		bind(new ClickMessageDecoder());
		bind(new FocusMessageDecoder());
		bind(new CameraMessageDecoder());
		bind(new FlagsMessageDecoder());
		bind(new SequenceNumberMessageDecoder());
		bind(new InterfaceClosedMessageDecoder());
		bind(new ObjectOptionOneMessageDecoder());
		bind(new ObjectOptionTwoMessageDecoder());

		/* encoders */
		bind(new RegionChangeMessageEncoder(table));
		bind(new InterfaceRootMessageEncoder());
		bind(new InterfaceOpenMessageEncoder());
		bind(new InterfaceCloseMessageEncoder());
		bind(new InterfaceVisibleMessageEncoder());
		bind(new InterfaceTextMessageEncoder());
		bind(new ServerMessageEncoder());
		bind(new LogoutMessageEncoder());
		bind(new PlayerUpdateMessageEncoder());
		bind(new SkillMessageEncoder());
		bind(new EnergyMessageEncoder());
		bind(new InterfaceItemsMessageEncoder());
		bind(new InterfaceSlottedItemsMessageEncoder());
		bind(new InterfaceResetItemsMessageEncoder());
		bind(new ResetMinimapFlagMessageEncoder());
		bind(new ConfigMessageEncoder());
		bind(new ScriptMessageEncoder());
		bind(new NpcUpdateMessageEncoder());
		bind(new ScriptStringMessageEncoder());
		bind(new ScriptIntMessageEncoder());
	}

	public MessageDecoder<?> get(int opcode) {
		return inCodecs[opcode];
	}

	@SuppressWarnings("unchecked")
	public <T extends Message> MessageEncoder<T> get(Class<T> clazz) {
		return (MessageEncoder<T>) outCodecs.get(clazz);
	}

	public void bind(MessageDecoder<?> decoder) {
		inCodecs[decoder.opcode] = decoder;
	}

	public void bind(MessageEncoder<?> encoder) {
		outCodecs.put(encoder.clazz, encoder);
	}

}
