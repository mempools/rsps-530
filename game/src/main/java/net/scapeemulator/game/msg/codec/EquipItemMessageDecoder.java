package net.scapeemulator.game.msg.codec;

import net.scapeemulator.game.msg.EquipItemMessage;
import net.scapeemulator.game.net.game.*;

import java.io.IOException;

public final class EquipItemMessageDecoder extends MessageDecoder<EquipItemMessage> {

	public EquipItemMessageDecoder() {
		super(55);
	}

	@Override
	public EquipItemMessage decode(GameFrame frame) throws IOException {
		GameFrameReader reader = new GameFrameReader(frame);
		int itemId = (int) reader.getUnsigned(DataType.SHORT, DataOrder.LITTLE);
		int itemSlot = (int) reader.getUnsigned(DataType.SHORT, DataTransformation.ADD);
		int inter = (int) reader.getSigned(DataType.INT, DataOrder.MIDDLE);
		int id = (inter >> 16) & 0xFFFF;
		int slot = inter & 0xFFFF;
		return new EquipItemMessage(id, slot, itemSlot, itemId);
	}

}
