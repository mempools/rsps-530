package net.scapeemulator.game.command;

import net.scapeemulator.game.model.Direction;
import net.scapeemulator.game.model.Player;
import net.scapeemulator.game.model.Position;
import net.scapeemulator.game.model.World;
import net.scapeemulator.game.model.pathfinding.AStarPathFinder;
import net.scapeemulator.game.model.pathfinding.Path;
import net.scapeemulator.game.task.Task;

public final class PositionCommandHandler extends CommandHandler {

    public PositionCommandHandler() {
        super("pos");
    }

    @Override
    public void handle(final Player player, String[] arguments) {
        if (player.getRights() < 2)
            return;

        if (arguments.length != 0) {
            final int x = Integer.parseInt(arguments[0]);
            final int y = Integer.parseInt(arguments[1]);

            player.sendMessage("Walking to: " + x + " " + y);

            boolean path1 = Direction.projectileClipping(player.getPosition(), new Position(x, y));
            boolean projectilepath = Direction.isTraversable(player.getPosition(), Direction.between(player.getPosition(), new Position(x, y, player.getPosition().getHeight())),player.getSize());
            player.sendMessage("Path1: "+path1 + " PP: "+projectilepath);
            return;
        }

//        if (arguments.length != 0) {
//            player.sendMessage("Syntax: ::pos");
//            return;
//        }

        Position position = player.getPosition();
        player.sendMessage("You are at: " + position.getX() + ", " + position.getY() + ", " + position.getHeight());
    }

}
